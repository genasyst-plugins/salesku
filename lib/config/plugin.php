<?php
return array(
    'name'     => 'Выбор характеристик и опций в списках',
    'version'  => '2.2.1',
    'critical'    => '2.2.1',
    'img'=> 'img/salesku.png',
    'vendor'   => '990614',
    'author' => 'Genasyst',
    'shop_settings' => true,
    'frontend' => true,
    'custom_settings' => true,
    'handlers' =>  array(
        'frontend_head' => 'frontendHead',
        'frontend_products' => 'frontendProducts',
    ),
);